create table if not exists items (
	id bigserial not null,
	name varchar(200) not null,
	price_in_eur decimal(12, 2) not null,

	primary key (id)
);

create table if not exists users (
	id bigserial not null,
	display_name varchar(200) not null,
	preferred_currency char(3) not null,

	primary key (id),
	unique (display_name)
);