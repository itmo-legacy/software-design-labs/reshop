function listenToStream(streamName, onmessage) {
	const eventSource = new EventSource("/api/v1/" + streamName)
	eventSource.onerror = function (event) { console.log("closing ${streamName}"); eventSource.close() }
	eventSource.onmessage = onmessage
}