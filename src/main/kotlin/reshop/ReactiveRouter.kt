package reshop

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.router

@Configuration
class ReactiveRouter {
    @Bean
    fun route(reactiveHandler: ReactiveHandler) = router {
        GET("/") { ok().render("items") }

        "/items".nest {
            GET("/") { ok().render("items") }
            GET("/new") { ok().render("add-item") }
            POST("/new", reactiveHandler::addNewItem)
        }

        GET("/enter") { ok().render("enter") }
        POST("/enter", reactiveHandler::enterAsUser)

        GET("/register") { ok().render("register") }
        POST("/register", reactiveHandler::registerUser)

        GET("/logout", reactiveHandler::logout)

        "/api/v1".nest {
            accept(MediaType.TEXT_EVENT_STREAM).nest {
                GET("/items", reactiveHandler::apiListItems)
                GET("/currencies", reactiveHandler::apiListCurrencies)
                GET("/preferred-currency", reactiveHandler::apiPreferredCurrency)
                GET("/user", reactiveHandler::apiUser)
                GET("/user-names", reactiveHandler::apiUserNames)
            }
        }
    }
}