package reshop.repository

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reshop.domain.Item

interface ItemRepository : ReactiveCrudRepository<Item, Long>