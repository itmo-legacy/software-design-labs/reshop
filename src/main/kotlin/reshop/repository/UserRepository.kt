package reshop.repository

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Mono
import reshop.domain.User

interface UserRepository : ReactiveCrudRepository<User, Long> {
    fun findByDisplayName(displayName: String): Mono<User>
}