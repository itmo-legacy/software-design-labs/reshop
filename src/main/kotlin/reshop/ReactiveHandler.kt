package reshop

import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.body
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import reshop.domain.Currency
import reshop.domain.ItemWithCurrencyView
import reshop.domain.User
import reshop.repository.ItemRepository
import reshop.repository.UserRepository

const val USER_ID_SESSION_KEY = "userId"

@Component
class ReactiveHandler(
    val itemRepository: ItemRepository,
    val userRepository: UserRepository,
) {
    fun apiListItems(request: ServerRequest): Mono<ServerResponse> {
        val body = getPreferredCurrency(request)
            .flatMapMany { currency ->
                itemRepository.findAll().map {
                    System.err.println(it)
                    ItemWithCurrencyView.fromItem(it, currency)
                }
            }

        return ServerResponse.ok().contentType(MediaType.TEXT_EVENT_STREAM).body(body)
    }

    fun apiListCurrencies(request: ServerRequest): Mono<ServerResponse> {
        return ServerResponse.ok().contentType(MediaType.TEXT_EVENT_STREAM)
            .body(Flux.fromArray(Currency.values()))
    }

    fun apiPreferredCurrency(request: ServerRequest): Mono<ServerResponse> {
        val body = getPreferredCurrency(request)
        return ServerResponse.ok().contentType(MediaType.TEXT_EVENT_STREAM).body(body)
    }

    fun apiUser(request: ServerRequest): Mono<ServerResponse> {
        val body = getUser(request)
        return ServerResponse.ok().contentType(MediaType.TEXT_EVENT_STREAM).body(body)
    }

    fun apiUserNames(request: ServerRequest): Mono<ServerResponse> {
        val body = userRepository.findAll().map(User::displayName)
        return ServerResponse.ok().contentType(MediaType.TEXT_EVENT_STREAM).body(body)
    }

    fun addNewItem(request: ServerRequest): Mono<ServerResponse> {
        return request
            .formData()
            .flatMap {
                val itemWithCurrencyView = ItemWithCurrencyView(
                    name = it.getFirst("item-name")!!,
                    price = it.getFirst("item-price")!!.toBigDecimal(),
                    currency = Currency.valueOf(it.getFirst("item-currency")!!)
                )
                val item = itemWithCurrencyView.toItem()
                itemRepository.save(item)
            }
            .flatMap { ServerResponse.ok().render("redirect:/") }
    }

    fun enterAsUser(request: ServerRequest): Mono<ServerResponse> {
        return request
            .formData()
            .flatMap { userRepository.findByDisplayName(it.getFirst("user-name")!!) }
            .flatMap { user -> request.session().map { it.attributes[USER_ID_SESSION_KEY] = user.id } }
            .flatMap { ServerResponse.ok().render("redirect:/") }
    }

    fun registerUser(request: ServerRequest): Mono<ServerResponse> {
        return request
            .formData()
            .flatMap {
                userRepository.save(User(
                    displayName = it.getFirst("user-name")!!,
                    preferredCurrency = Currency.valueOf(it.getFirst("user-currency")!!)
                ))
            }
            .flatMap { user -> request.session().map { it.attributes[USER_ID_SESSION_KEY] = user.id } }
            .flatMap { ServerResponse.ok().render("redirect:/") }
    }

    fun logout(request: ServerRequest): Mono<ServerResponse> {
        return request
            .session()
            .map { it.attributes.remove(USER_ID_SESSION_KEY) }
            .flatMap { ServerResponse.ok().render("redirect:/") }
    }

    private fun getPreferredCurrency(request: ServerRequest): Mono<Currency> {
        return getUser(request)
            .flatMap { user -> user.preferredCurrency.toMono() }
            .switchIfEmpty(Currency.EUR.toMono())
    }

    private fun getUser(request: ServerRequest): Mono<User> {
        return request.session()
            .flatMap { session -> session.getAttribute<Long>(USER_ID_SESSION_KEY).toMono() }
            .flatMap { id -> userRepository.findById(id) }
    }
}

