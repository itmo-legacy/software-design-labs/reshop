package reshop.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("users")
data class User(
    val displayName: String,
    val preferredCurrency: Currency,
    @Id val id: Long = 0,
)