package reshop.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import java.math.BigDecimal

@Table("items")
data class Item(
    val name: String,
    val priceInEur: BigDecimal,
    @Id val id: Long = 0,
)