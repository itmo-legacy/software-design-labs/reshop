package reshop.domain

import java.math.BigDecimal
import java.math.MathContext

enum class Currency(private val perEurRate: BigDecimal) {
    EUR(BigDecimal.ONE),
    USD(BigDecimal("1.2")),
    RUB(BigDecimal("87.93"));

    fun convertFromEur(amountInEur: BigDecimal): BigDecimal =
        amountInEur.multiply(perEurRate, MathContext.DECIMAL128)

    fun convertToEur(amountInThis: BigDecimal): BigDecimal =
        amountInThis.divide(perEurRate, MathContext.DECIMAL128)
}