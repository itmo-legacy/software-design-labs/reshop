package reshop.domain

import java.math.BigDecimal

data class ItemWithCurrencyView(
    val name: String, val price: BigDecimal, val currency: Currency,
) {
    fun toItem() = Item(name, currency.convertToEur(price))

    companion object {
        fun fromItem(item: Item, currency: Currency) =
            ItemWithCurrencyView(
                name = item.name,
                price = currency.convertFromEur(item.priceInEur),
                currency = currency
            )
    }
}